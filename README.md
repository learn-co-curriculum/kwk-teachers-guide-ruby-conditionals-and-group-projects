## Objectives

1. Students should review Ruby Arrays and Hashes
2. Students should be able to write conditional statements to control the flow of their Ruby methods
2. Students will work in groups on a project to build a command line calculator

## Resources

### Conditionals

* [Intro to Conditionals](https://github.com/learn-co-curriculum/conditional-readme)
* [Video - Conditionals](https://www.youtube.com/embed/dcNgPOZCaBk)
* [If Else Statements](https://www.w3resource.com/ruby/ruby-if-else-unless.php)
* [Ruby for Beginners Conditionals Reference](http://ruby-for-beginners.rubymonstas.org/conditionals.html)
* [Conditionals, Modifiers and Switch](https://www.tutorialspoint.com/ruby/ruby_if_else.htm)

### Pair Programming on Projects

* [How to Pair Program](https://www.youtube.com/watch?v=YhV4TaZaB84)
* [Pair Programming Tips](https://www.tutorialspoint.com/extreme_programming/extreme_programming_pair_programming.htm)
